<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */
namespace Pgdev\Cell\ValidatorExceptions;

use Exception;

class ValidatorException extends Exception
{

}